## Application flow:

Standalone application to create unique user id.

## Technologies used:

- [Java] (https://www.oracle.com/java/index.html)
- [Maven](http://maven.apache.org/)
- [JUnit](http://junit.org/junit4/)

## Getting Started

To get started, please install [Java] (https://www.oracle.com/java/index.html) and [Maven](http://maven.apache.org/install.html) on you local machine. Download or clone code from bitbucket server.

```
git clone https://rakeshprajapati@bitbucket.org/rakeshprajapati/dto.git
cd dto
mvn package
cd target
java -jar dto-1.0-SNAPSHOT.jar
```
This will install the dependencies for project and run application.
