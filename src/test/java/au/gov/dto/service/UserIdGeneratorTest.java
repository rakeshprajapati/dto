package au.gov.dto.service;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class UserIdGeneratorTest {

    @Test
    public void test_GetUniqueId_For_DifferentAccountType() throws Exception {
        UserIdGenerator facebookIdWithInitech = new UserIdGenerator("facebook,initech,jess1234");
        UserIdGenerator googleIdWithInitech = new UserIdGenerator("google,initech,jess1234");
        UserIdGenerator localWithInitech = new UserIdGenerator("local,initech,jess1234");

        assertThat("Unique id for Google and Facebook type should not match same business and user", googleIdWithInitech.getUniqueId(), not(equalTo(facebookIdWithInitech.getUniqueId())));
        assertThat("Unique id for Google and Local and Facebook type should not match with same business and user", localWithInitech.getUniqueId(), not(equalTo(facebookIdWithInitech.getUniqueId())));
        assertThat("Unique id for Google and Local and Google type should not match with same business and user", localWithInitech.getUniqueId(), not(equalTo(googleIdWithInitech.getUniqueId())));
    }

    @Test
    public void test_GetUniqueId_For_DifferentUserId() throws Exception {
        UserIdGenerator facebookIdWithInitech = new UserIdGenerator("facebook,initech,jess1234");
        UserIdGenerator idGenerator = new UserIdGenerator("facebook,initech,jses1234");
        assertThat("Test by changing characters in user id", idGenerator.getUniqueId(), not(equalTo(facebookIdWithInitech.getUniqueId())));
        UserIdGenerator idGenerator1 = new UserIdGenerator("facebook,initech,jses1243");
        assertThat("Test by changing characters in user id", idGenerator1.getUniqueId(), not(equalTo(facebookIdWithInitech.getUniqueId())));
        assertThat("Test by changing characters in user id", idGenerator1.getUniqueId(), not(equalTo(idGenerator.getUniqueId())));
    }

    @Test
    public void test_GetUniqueId_For_DifferentBussinessType() throws Exception {
        UserIdGenerator initechIdGenerator = new UserIdGenerator("facebook,initech,jess1234");
        UserIdGenerator initrodeIdGenerator = new UserIdGenerator("facebook,initrode,jses1234");
        assertThat("Test by changing characters in user id", initrodeIdGenerator.getUniqueId(), not(equalTo(initechIdGenerator.getUniqueId())));
        UserIdGenerator idGenerator1 = new UserIdGenerator("facebook,random,jses1243");
        assertThat("Test by changing characters in user id", idGenerator1.getUniqueId(), not(equalTo(initechIdGenerator.getUniqueId())));
        assertThat("Test by changing characters in user id", idGenerator1.getUniqueId(), not(equalTo(initrodeIdGenerator.getUniqueId())));
    }

}