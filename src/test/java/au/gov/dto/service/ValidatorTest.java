package au.gov.dto.service;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test case for validator
 */
public class ValidatorTest {

    private Validator validator = new Validator();

    @Test
    public void inputValidate() throws Exception {
        assertThat(validator.inputValidate(""), is(false));
        assertThat(validator.inputValidate("facebook"), is(false));
        assertThat(validator.inputValidate("facebook,initech"), is(false));
        assertThat(validator.inputValidate("facebook,initech, "), is(false));
        assertThat(validator.inputValidate("facebook,initech,jess1234"), is(true));
        assertThat(validator.inputValidate("facebook,initrode,jess1234"), is(true));
        assertThat(validator.inputValidate("google,initrode,jess1234"), is(true));
        assertThat(validator.inputValidate("google,initech,jess1234"), is(true));
        assertThat(validator.inputValidate("local,initech,jess1234"), is(true));
        assertThat(validator.inputValidate("local,initrode,jess1234"), is(true));
        assertThat(validator.inputValidate("faceBook,invalid,jess1234"), is(false));
        assertThat(validator.inputValidate("faceBook,invalid,jess1234"), is(false));
        assertThat(validator.inputValidate("invalud,initrode,jess1234"), is(false));
    }
}