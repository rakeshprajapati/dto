package au.gov.dto;

import au.gov.dto.service.UserIdGenerator;
import au.gov.dto.service.Validator;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * DTO main class to start utility
 */
public class Main {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        Validator validator = new Validator();
        while (true) {
            int ch;
            System.out.println("Enter some text: ");
            String input = "";
            while ((ch = System.in.read()) != '\n') {
                input = input + (char) ch;
            }
            if ("exit".equals(input.toLowerCase().trim())) {
                System.out.print("See you next time!");
                System.exit(0);
            }
            if (!validator.inputValidate(input)) {
                System.out.println("error");
            } else {
                UserIdGenerator generator = new UserIdGenerator(input);
                System.out.println(input + "," + generator.getUniqueId());
            }
        }
    }
}
