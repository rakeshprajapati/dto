package au.gov.dto.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * User Id generator, it requires
 */
public class UserIdGenerator {
    private String input;

    /**
     * @param input
     */
    public UserIdGenerator(String input) {
        this.input = input.toUpperCase();
    }

    public String getUniqueId() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(this.input.getBytes(), 0, this.input.length());
        byte[] bytes = md.digest();
        StringBuffer userId = new StringBuffer();
        for (int i = 0; i < 8; i++) {
            userId.append(Integer.toHexString(0xFF & bytes[i]));
        }
        return userId.toString();
    }
}
