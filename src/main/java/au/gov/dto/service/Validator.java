package au.gov.dto.service;

import au.gov.dto.AccountType;
import au.gov.dto.Business;

/**
 * Class implements the validation for input. It Should have been interface, however no need to complete things here
 */
public class Validator {

    /**
     * Validate input string
     *
     * @param input
     * @return true if value is
     */
    public boolean inputValidate(String input) {
        if (input != null) {
            String[] inArray = input.split(",");
            return inArray.length == 3
                    && supportedAccountType(inArray[0])
                    && supportedBusiness(inArray[1])
                    && !"".equals(inArray[2].trim());
        }
        return false;
    }


    /**
     * Method validates the account type is supported or not
     * @param value
     * @return true if supported account type else false
     */
    private boolean supportedAccountType(String value) {
        try {
            AccountType.valueOf(value.toUpperCase());
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * Method validates the Business is supported or not
     * @param value
     * @return true if valid bussiness type else false
     */
    private boolean supportedBusiness(String value) {
        try {
            Business.valueOf(value.toUpperCase());
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
