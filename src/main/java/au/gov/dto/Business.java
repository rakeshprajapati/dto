package au.gov.dto;

/**
 * Enum supporting businesses
 */
public enum Business {
    INITECH,
    INITRODE
}
