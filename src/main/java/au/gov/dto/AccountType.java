package au.gov.dto;

/**
 *
 */
public enum AccountType {
    FACEBOOK,
    GOOGLE,
    LOCAL;
}
